package interviewtest

import (
	"fmt"
	"math"
	"testing"
)

/**
Find smallest number with given number of digits and sum of digits
How to find the smallest number with given digit sum s and number of digits d?
Examples:
Input  : s = 9, d = 2
Output : 18
There are many other possible numbers
like 45, 54, 90, etc with sum of digits
as 9 and number of digits as 2. The
smallest of them is 18.

Input  : s = 20, d = 3
Output : 299
*/
func i3_FindSmallestNumber(sum, number int) int {
	nArr := make([]int, number)
	nIndex := number - 1
	sum -= 1

	for nIndex > 0 {
		if sum > 9 {
			nArr[nIndex] = 9
			sum -= 9
		} else {
			nArr[nIndex] = sum
			sum = 0
		}
		nIndex--
	}
	nArr[0] = 1
	var smallestNumber int
	l := number - 1
	for _, n := range nArr {
		smallestNumber += n * int(math.Pow10(l))
		l--
	}

	return smallestNumber
}

func TestFindSmallestNumber(t *testing.T) {
	s, n := 9, 2
	smallestNum := i3_FindSmallestNumber(s, n)
	fmt.Println("smallest number: ", smallestNum)
}
