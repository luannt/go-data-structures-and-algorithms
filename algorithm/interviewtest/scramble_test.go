package interviewtest

import (
	"fmt"
	"strings"
	"testing"
)

/**
https://www.geeksforgeeks.org/check-if-a-string-is-a-scrambled-form-of-another-string/?ref=leftbar-rightbar
Check if a string is a scrambled form of another string
In order to solve this problem, we are using Divide and Conquer approach.
Given two strings of equal length (say n+1), S1[0…n] and S2[0…n].
If S2 is a scrambled form of S1, then there must exist an index i such
that at least one of the following conditions is true:

S2[0…i] is a scrambled string of S1[0…i] and S2[i+1…n] is a scrambled string of S1[i+1…n].
S2[0…i] is a scrambled string of S1[n-i…n] and S2[i+1…n] is a scrambled string of S1[0…n-i-1].
*/
func checkScramble(strArr1, strArr2 []string) bool {
	n := len(strArr1)
	if n == 0 {
		return true
	}
	str1, str2 := strings.Join(strArr1, ""), strings.Join(strArr2, "")
	fmt.Println("str1: ", str1, "str2: ", str2)
	if str1 == str2 {
		return true
	}
	//Check strArr2 and strArr1 is anagram with each other
	for i := 1; i < n; i++ {
		//Check S2[0…i] is a scrambled string of S1[0…i] and S2[i+1…n] is a scrambled string of S1[i+1…n].
		if checkScramble(strArr1[:i], strArr2[:i]) && checkScramble(strArr1[i:], strArr2[i:]) {
			return true
		}
		//Check S2[0…i] is a scrambled string of S1[n-i…n] and S2[i+1…n] is a scrambled string of S1[0…n-i-1].
		if checkScramble(strArr1[:i], strArr2[n-i:]) && checkScramble(strArr1[i:], strArr2[:n-i]) {
			return true
		}
	}
	return false
}

func TestScramble(t *testing.T) {
	//TODO: need improve
	str1 := "abcde"
	str2 := "caebd"
	isScrambled := checkScramble(splitString(str1), splitString(str2))
	fmt.Printf("check %s is scrambled %s: %v\n", str2, str1, isScrambled)
}
