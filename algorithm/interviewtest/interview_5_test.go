package interviewtest

import (
	"fmt"
	"testing"
)

/**
https://www.geeksforgeeks.org/search-a-word-in-a-2d-grid-of-characters/?fbclid=IwAR2wc1v-eJHmyDHGpdOFjX3v1ievTtQLscCAEZjy1CUsH0yyyPOFI3vqnVY
Search a Word in a 2D Grid of characters
Given a 2D grid of characters and a word, find all occurrences of the given word in the grid. A word can be matched in all 8 directions at any point. Word is said to be found in a direction if all characters match in this direction (not in zig-zag form).

The 8 directions are, Horizontally Left, Horizontally Right, Vertically Up and 4 Diagonal directions.
Examples:
Input:  grid[][] = {"GEEKSFORGEEKS",
                    "GEEKSQUIZGEEK",
                    "IDEQAPRACTICE"};
        word = "GEEKS"

Output: pattern found at 0, 0
        pattern found at 0, 8
        pattern found at 1, 0
Explanation: 'GEEKS' can be found as prefix of
1st 2 rows and suffix of first row
*/
func i5SearchWord(matrix [][]string, word string) [][2]int {
	wordArr := splitString(word)
	fmt.Println("split word: ", wordArr)
	var result [][2]int
	for r, cells := range matrix {
		fmt.Printf("check row %d, cells: %v\n", r, wordArr)
		for c, s := range cells {
			fmt.Printf("check chell %d, value: %s, first word: %s\n", c, s, wordArr[0])
			if s == wordArr[0] {
				fmt.Println("start search pattern")
				if i5MatchPattern(matrix, r, c, wordArr) {
					result = append(result, [2]int{r, c})
				}
			}
		}
	}
	return result
}

func i5MatchPattern(matrix [][]string, r, c int, words []string) bool {
	dir := [][]int{
		{-1, -1}, {-1, 0}, {-1, 1},
		{0, -1}, {0, 1},
		{1, -1}, {1, 0}, {1, 1},
	}
	for _, v := range dir {
		x, y := r+v[0], c+v[1]
		fmt.Printf("vector x,y = %d,%d\n", x, y)
		for i := 1; i < len(words); i++ {
			fmt.Println("start check match word", "x:", x, "y:", y)
			if x >= len(matrix) || x < 0 {
				break
			}
			if y >= len(matrix[x]) || y < 0 {
				break
			}
			if matrix[x][y] != words[i] {
				break
			}
			if i == len(words)-1 {
				return true
			}
			fmt.Println("match word: ", words[i])
			x, y = x+v[0], y+v[1]
		}
	}
	return false
}

func TestI5SearchWordMatrix(t *testing.T) {
	matrix := [][]string{
		{"G", "E", "E", "K", "S", "F", "O", "R", "G", "E", "E", "K", "S"},
		{"G", "E", "E", "K", "S", "Q", "U", "I", "Z", "G", "E", "E", "K"},
		{"I", "D", "E", "Q", "A", "P", "R", "A", "C", "T", "I", "C", "E"},
	}
	result := i5SearchWord(matrix, "GEEKS")
	fmt.Println("pattern found at:", result)
}
