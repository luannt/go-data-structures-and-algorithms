package interviewtest

import (
	"fmt"
	"testing"
)

/**
Given an array of integers of size ‘n’.
Our aim is to calculate the maximum sum of ‘k’
consecutive elements in the array.

Input  : arr[] = {100, 200, 300, 400}
         k = 2
Output : 700
*/
func i4FindMaxSumConsElement(arr []int, k int) (int, []int) {
	if k > len(arr) {
		return -1, nil
	}
	max := 0
	var subkArr []int
	for i := 0; i <= len(arr)-k; i++ {
		subArr := arr[i : i+k]
		fmt.Println("sub array: ", subArr)
		sum := i4SumArr(subArr)
		if sum > max {
			max = sum
			subkArr = subArr
		}
	}
	return max, subkArr
}
func i4SumArr(arr []int) int {
	sum := 0
	for _, a := range arr {
		sum += a
	}
	return sum
}

func TestI4FindMaxSumConsElement(t *testing.T) {
	arr := []int{1, 4, 2, 10, 23, 3, 1, 0, 20}
	k := 4
	max, subArr := i4FindMaxSumConsElement(arr, k)
	fmt.Println("maximum sum of ‘k’ consecutive elements in the array: ", max)
	fmt.Println("Sub array: ", subArr)
}
