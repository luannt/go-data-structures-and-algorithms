package interviewtest

import (
	"fmt"
	"sort"
	"strings"
	"testing"
)

/**
Check whether two strings are anagram of each other
Write a function to check whether two given strings are anagram of each other or not.
An anagram of a string is another string that contains same characters, only the order of characters can be different.
For example, “abcd” and “dabc” are anagram of each other.
*/

/**
Method: 1
Step 1: sort 2 strings
step 2: compare 2 strings
*/
func checkAnagram_1(str1, str2 string) bool {
	if len(str1) != len(str2) {
		return false
	}
	sortStr1 := sortString(str1)
	sortStr2 := sortString(str2)
	return sortStr1 == sortStr2
}

func sortString(str string) string {
	strArr := splitString(str)
	//Sort StrArr, here we'll sort by sort pkg
	sort.SliceStable(strArr, func(i, j int) bool {
		return strArr[i] < strArr[j]
	})
	return strings.Join(strArr, "")
}
func splitString(str string) []string {
	strArr := make([]string, len(str))
	for i, c := range str {
		strArr[i] = string(c)
	}
	return strArr
}

/**
Method 2: count character of strings element and compare it
Example: aacb => count1[a]=2, count1[b]=1, count1[c]=1
Example: acba => count2[a]=2, count2[b]=1, count1[c]=1
Compare count1 and count2
*/
func checkAnagram_2(str1, str2 string) bool {
	strArr1 := splitString(str1)
	countMap1 := strCount(strArr1)
	strArr2 := splitString(str2)
	countMap2 := strCount(strArr2)
	for _, s := range strArr1 {
		if countMap1[s] != countMap2[s] {
			return false
		}
	}
	return true
}

func strCount(str []string) map[string]int {
	m := make(map[string]int, len(str))
	for _, s := range str {
		m[s]++
	}
	return m
}

func TestAnagram_1(t *testing.T) {
	str1 := "anagram"
	str2 := "gramana"
	isAnagram := checkAnagram_1(str1, str2)
	fmt.Println(str1, "and", str2, "is anagram: ", isAnagram)
}

func TestAnagram_2(t *testing.T) {
	str1 := "anagram"
	str2 := "gramana"
	isAnagram := checkAnagram_2(str1, str2)
	fmt.Println(str1, "and", str2, "is anagram: ", isAnagram)
}
