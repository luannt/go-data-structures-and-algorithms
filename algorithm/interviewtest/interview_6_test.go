package interviewtest

import (
	"fmt"
	"testing"
)

/**
https://www.geeksforgeeks.org/check-if-a-word-exists-in-a-grid-or-not/?ref=rp
Check if a word exists in a grid or not
Given a 2D grid of characters and a word, the task is to check if that word exists in the grid or not. A word can be matched in 4 directions at any point.
The 4 directions are, Horizontally Left and Right, Vertically Up and Down.
Examples:
Input:  grid[][] = {"axmy",
                    "bgdf",
                    "xeet",
                    "raks"};
		word: geeks
Output: Yes

Input: grid[][] = {"axmy",
                   "brdf",
                   "xeet",
                   "rass"};
		word: geeks
Output : No
*/
func i6SearchWord(matrix [][]string, word string) bool {
	wordArr := splitString(word)
	fmt.Println("split word: ", wordArr)
	markCellSet := map[int]map[int]bool{}
	for r, cells := range matrix {
		fmt.Printf("check row %d, cells: %v\n", r, wordArr)
		for c, s := range cells {
			fmt.Printf("check chell %d, value: %s, first word: %s\n", c, s, wordArr[0])
			if s == wordArr[0] {
				fmt.Println("start search pattern")
				markCellSet = i6MarkCell(markCellSet, r, c, true)
				if i6MatchPattern(markCellSet, matrix, r, c, wordArr[1:]) {
					fmt.Println("mark cell set:", markCellSet)
					return true
				}
				markCellSet = i6MarkCell(markCellSet, r, c, false)
			}
		}
	}
	fmt.Println("mark cell set:", markCellSet)
	return false
}

func i6MarkCell(markCellSet map[int]map[int]bool, r, c int, mark bool) map[int]map[int]bool {
	m, ok := markCellSet[r]
	if ok {
		m[c] = mark
	} else {
		m = map[int]bool{c: mark}
	}
	markCellSet[r] = m
	return markCellSet
}

func i6MatchPattern(markSet map[int]map[int]bool, matrix [][]string, r, c int, words []string) bool {
	if len(words) == 0 {
		return true
	}
	fmt.Println("next word to check ", words)
	dir := [][]int{
		{-1, 0}, {0, -1},
		{0, 1}, {1, 0},
	}
	for _, v := range dir {
		x, y := r+v[0], c+v[1]
		if markSet[x][y] == true {
			fmt.Println("cell is mark, continue")
			continue
		}
		fmt.Printf("vector x,y = %d,%d\n", x, y)
		if x >= len(matrix) || x < 0 {
			break
		}
		if y >= len(matrix[x]) || y < 0 {
			break
		}
		if matrix[x][y] != words[0] {
			continue
		}
		fmt.Printf("found cell at: x,y = %d, %d\n", x, y)
		markSet = i6MarkCell(markSet, r, c, true)
		if i6MatchPattern(markSet, matrix, x, y, words[1:]) {
			return true
		}
		markSet = i6MarkCell(markSet, r, c, false)
	}
	return false
}

func TestI6SearchWordMatrix(t *testing.T) {
	matrix := [][]string{
		{"A", "X", "M", "Y"},
		{"B", "G", "D", "F"},
		{"X", "E", "E", "T"},
		{"K", "A", "K", "S"},
	}
	result := i6SearchWord(matrix, "GEEKS")
	fmt.Println("word exist in matrix: ", result)
}
