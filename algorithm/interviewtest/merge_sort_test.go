package interviewtest

import (
	"fmt"
	"testing"
)

func mergeSort(arr []int) []int {
	fmt.Println("merge sort arr", arr)
	if len(arr) < 2 {
		return arr
	}
	mid := len(arr) / 2
	leftArr, rightArr := arr[:mid], arr[mid:]
	leftArr = mergeSort(leftArr)
	rightArr = mergeSort(rightArr)
	return merge(leftArr, rightArr)
}

func merge(leftArr, rightArr []int) []int {
	fmt.Println("left arr", leftArr)
	fmt.Println("right arr", rightArr)
	mergeArr := make([]int, 0, len(leftArr)+len(rightArr))
	for len(leftArr) > 0 && len(rightArr) > 0 {
		if leftArr[0] < rightArr[0] {
			mergeArr = append(mergeArr, leftArr[0])
			leftArr = leftArr[1:]
		} else {
			mergeArr = append(mergeArr, rightArr[0])
			rightArr = rightArr[1:]
		}
	}
	for j := range leftArr {
		mergeArr = append(mergeArr, leftArr[j])
	}
	for j := range rightArr {
		mergeArr = append(mergeArr, rightArr[j])
	}
	fmt.Println("array after merging", mergeArr)
	return mergeArr
}

func TestMergeSort(t *testing.T) {
	arr := []int{1, 5, 3, 6, 4, 9, 2}
	fmt.Println("array before sorting: ", arr)
	arr = mergeSort(arr)
	fmt.Println("array after sorting: ", arr)
}
